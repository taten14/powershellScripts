# Fuente:
# https://www.ghacks.net/2022/05/07/how-to-display-all-cached-dns-entries-on-windows/

ipconfig /displayDNS
Get-DnsClientCache | Format-Table -AutoSize

Get-DnsServerStatistics

# Fuente:
# https://ubuntu.forumming.com/question/7401/change-dns-server-given-during-ubuntu-18-04-installation

sudo lsof -i :53 -S

#lsof - list open files
#  -i [i]   selects the listing of files any of whose Internet address matches the address specified in i.  If no address is specified, this option selects the listing of all Internet and x.25 (HP-UX) network files.
#  -S [t]   specifies  an  optional  time-out seconds value for kernel functions - lstat(2), readlink(2), and stat(2) - that might otherwise deadlock.  The minimum for t is two; the default, fifteen; when no value is specified, the default is used.


systemd-resolve --status

# In order to check that the DNS cache was actually flushed, you can send a “USR1” signal to the systemd-resolved service. This way, it will dump its current state into the systemd journal.

sudo killall -USR1 systemd-resolved

sudo pkill -USR1 systemd-resolve

# Ahora volcamos el contenido y vemos

sudo journalctl -u systemd-resolved > ~/resolved.txt

