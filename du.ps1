# Author: Dante Paniagua <danterpaniagua@gmail.com>

function du {
    param(
        [Parameter()][String]$Path

    )
    $Folders=(Get-ChildItem -Directory $Path).FullName
    $carpetas= @()
    foreach ($Folder in $Folders){
        #write-host $folder
        $carpeta=New-Object -TypeName psobject -Property @{
                Carpeta = $Folder
                Espacio = '{0:N4} GB' -f ((gci -force $Folder –Recurse -ErrorAction SilentlyContinue | measure-object -property Length -s).sum / 1Gb)
            }
        $carpetas+=$carpeta
        }
    $carpetas |  Select-Object Carpeta, Espacio | Format-Table -AutoSize
}