# Funcion para obtener los usuarios logueados en una maquina
# Por defecto analiza todas las vms y lo almacena en el home del usuario que ejecuta con el nombre LoggedUser.csv
# Se debe correr como admin desde el AD

#Requires -RunAsAdministrator

function Get-LoggedUsers{
    param(
        [Parameter()][String]$HostName
        #[Parameter()][String]$OutputPath,
        #[Parameter(ParameterSetName="Hostnames")][String[]]$HostNames
    )
    $resultado = @()
    if ( !($HostName -eq "" ) ) { $Computers = Get-ADComputer -Filter 'Name -eq $HostName' -Properties * | Select Name,OperatingSystem,IPv4Address, IPv6Address, LastLogonDate }
    else {$Computers = Get-ADComputer -Filter * -Properties * | Select Name,OperatingSystem,IPv4Address, IPv6Address, LastLogonDate}
    #$Computers = Get-ADComputer -Filter * | select Name
    #$Computers = Get-ADComputer -Filter * -Properties * | Select Name,OperatingSystem,IPv4Address, IPv6Address, LastLogonDate
    foreach ($Computer in $Computers){
        $LoggedUser= New-Object -TypeName psobject -Property @{
            ComputerName=$Computer.Name
            OperatingSystem=$Computer.OperatingSystem
            IPv4Address=$Computer.IPv4Address
            IPv6Address=$Computer.IPv6Address
            LastLogonDate=$Computer.LastLogonDate
            Usuario=(Get-WmiObject -Class win32_computersystem -ComputerName $Computer.Name).Username
            } | Select Computername,OperatingSystem,IPv4Address, IPv6Address, LastLogonDate,Usuario
        Write-Output "$($LoggedUser.ComputerName):$($LoggedUser.Usuario) logueado desde $($LoggedUser.LastLogonDate)"
        $resultado += $LoggedUser
        } $resultado | Export-Csv -Encoding Unicode -Path "c:\Users\$env:USERNAME\LoggedUser.csv"
}