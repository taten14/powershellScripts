$resultado = @()
$Groups = Get-ADGroup -filter * -ErrorAction SilentlyContinue | select name
Write-Output "Grupos: "$Groups.Count
foreach ($Group in $Groups) {
    $Members = Get-ADGroupMember -identity $Group.name -ErrorAction SilentlyContinue | select name,SamAccountName
    Write-Output "Usuarios en "$Group.name": "$Members.Count
    foreach ($Member in $Members){
        $miembro = New-Object -TypeName psobject -Property @{
            Username = $Member.name
            Name = $Member.SamAccountName
            UserGroup = $Group.name
        } | Select-Object Username, Name, UserGroup
        $resultado += $miembro
    }
} $resultado | Export-Csv -Encoding Unicode -Path GruposMiembros.csv