Import-Module ActiveDirectory

$ADComputer = Get-ADComputer -Filter {Enabled -eq $true -and Operatingsystem -like "*"} -Properties * | select name, operatingsystem

$Report = @()
foreach ($comp in $ADComputer)
{
	$servername = $Comp.Name
	$TestConnection = Test-Connection -ComputerName $servername -Count 1 -ErrorAction SilentlyContinue
	if ($TestConnection)
	{
        $ADData = Get-ADComputer -Identity "$servername" -Property * -ErrorAction SilentlyContinue | Select-Object -Property Name,IPv4Address,IPv6Address,OperatingSystem,OperatingSystemServicePack,OperatingSystemVersion,LastLogonDate,DNSHostName
		$computerName = Get-WmiObject win32_computersystem -ComputerName $servername -ErrorAction SilentlyContinue
		$OS = Get-WmiObject win32_operatingsystem -ComputerName $servername -ErrorAction SilentlyContinue
		$Bios = Get-WmiObject win32_bios -ComputerName $servername -ErrorAction SilentlyContinue
		$PhysicalRAM = (Get-WMIObject -class Win32_PhysicalMemory -ComputerName $servername -ErrorAction SilentlyContinue | Measure-Object -Property capacity -Sum | % { [Math]::Round(($_.sum / 1GB), 2) })
		$CPU = Get-WmiObject Win32_ComputerSystem -ComputerName $servername -ErrorAction SilentlyContinue | Select NumberOfLogicalProcessors, NumberOfProcessors, systemname, name
		$Proccesor = Get-WmiObject –class Win32_processor -ComputerName $servername -ErrorAction SilentlyContinue
		$Disk = Get-WmiObject Win32_logicaldisk -ComputerName $servername -ErrorAction SilentlyContinue | select @{Name="Drive Size(GB)";Expression={[decimal]("{0:N0}" -f($_.size/1gb))}},@{Name="Drive Free Space(GB)";Expression={[decimal]("{0:N0}" -f($_.freespace/1gb))}},	@{Name="Drive Free pct";Expression={"{0,6:P0}" -f(($_.freespace/1gb) / ($_.size/1gb))}}
		$Report += New-Object System.Management.Automation.PSObject -Property @{
			'System Name'    = $servername
			#'OS Name'		  = $OS.Caption
            'OS Name'		  = $ADData.OperatingSystem
			'Manufacturer'    = $computerName.Manufacturer
			'Model'		      = $computerName.Model
			'Serial Number'  = $Bios.SerialNumber
			'Total RAM (GB)'= $PhysicalRAM
            'CPU - NumberOfLogicalProcessors' = $CPU.NumberOfLogicalProcessors
            'CPU - NumberOfProcessors' = $CPU.NumberOfProcessors
            'CPU Model' = $Proccesor.Name
            'IP Address' = $ADData.IPv4Address
            'OS Version' = $ADData.OperatingSystemVersion
            'OS Service Pack' = $ADData.OperatingSystemServicePack
            'FQDN' = $ADData.DNSHostName
            }
	}

	else
     

	{
		Write-Warning "Computer Name: $servername Isn't Available"

	}
$Report  | export-csv u:\x4.CSV
}