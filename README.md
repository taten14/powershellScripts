# Scripts Powershell

Scripts generados para ayudar en las tareas diarias

## du

Script que emula al comando **du** de linux. Permite analizar el uso de las carpetas en un sistema de archivos.

### Uso

```powershell
du 'c:\Program Files'
```
![Resultado DU](img/du01.png)

## GetADPCInfo

Obtiene info de los ordenadores de un dominio

### Uso

TBD

## GetLoggedUsers

Encuentra los usuarios logueados en una maquina. Si no se le pasan paramteros analiza toda la red.
Guarda un CSV en el home del usuario.

### Uso

Se debe ejecutar con privilegios desde el AD

TBD

## GetAdUserMember

Itera por los grupos del tribunal y obtiene sus miembres.

## Uso

TBD

## GetWSUSData

Itera por los ordenadores obteniendo la ultima actualizacion que recibio desde WSUS

### Uso

TBD

## CambiarADPassword

Cambia el password del usuario en el AD

### Uso

## restore_wsus

Repara WSUS en maquinas que no se conectan

![Social Microsoft](https://social.technet.microsoft.com/Forums/sharepoint/en-US/bf3d80fc-e4cd-4883-bfa5-812539c59265/wsus-some-clients-cant-update?forum=winserverwsus)