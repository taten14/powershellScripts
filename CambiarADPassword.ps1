# Active Directory module for Windows PowerShell in Remote Server Administration Tools > Role Administration Tools > AD DS and AD LDS Tools.
import-module activedirectory
$cred = Get-Credential tribunalcba\$env:username
$username = $cred.username
$password = $cred.GetNetworkCredential().password
$CurrentDomain = "LDAP://" + ([ADSI]"").distinguishedName
$domain = New-Object System.DirectoryServices.DirectoryEntry($CurrentDomain,$UserName,$Password)

if ($domain.name -eq $null)
{
    write-host "Credenciales incorrectas para tribunalcba\$env:username."
    exit
}
else
{
    write-warning "Autenticado exitosamente con tribunalcba\$env:username"
    $Contra = read-host "Ingrese nueva contraseña" -AsSecureString
    $ReContra = Read-host "Repita contraseña" -AsSecureString
    if ($Contra.ToString() -eq $ReContra.ToString())
    {
        Write-Warning "Passwords coniciden, cambiando contraseña"
        Set-ADAccountPassword -Identity $env:username -NewPassword $Contra -Reset
    }
    else
    {
        write-host "Las contraseñas no coinciden..."
    }
}